# Neighbourhood Classification - Coursera Python Data Science Capstone Project

The python notebook ['Neighbourhood_Classification_Zuerich'](https://gitlab.ethz.ch/eliabs/neighbourhood-classification-coursera-python-data-science-capstone-project/-/blob/main/Neighbourhood_Classification_Zuerich.ipynb?ref_type=heads)) contains the workflow to extract data of neighbourhoods of certain cities using the FourScquare API. This information is then used in two different  classification models (Random Forests and K-Nearest-Neighbour) to classify the neighbourhoods of another city (Zuerich) into 'boring' and 'favourite'.

The pdf file ['Neighbourhood_Classification_Presentation'](https://gitlab.ethz.ch/eliabs/neighbourhood-classification-coursera-python-data-science-capstone-project/-/blob/main/Neighbourhood_Classification_Presentation.pdf?ref_type=heads) contains a summary of the project including the results. 

This project was part of the Coursera IBM Applied Data Science Capstone (https://www.coursera.org/account/accomplishments/verify/B2GR3SBZUQ7R).



